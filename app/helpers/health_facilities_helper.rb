module HealthFacilitiesHelper
  
  def selected_tags?(type)
      return true unless params.has_key?(:typeOf)
      params[:typeOf].include?(type)
    end
end
