    class FacilityManagementController < ApplicationController
    require 'rest-client'
    require 'mime-types'
    require 'netrc'
    require 'http-cookie'
    require 'open-uri'
    def index
     @fac_array = get_Health_Json["resources"]
     @capacities = get_facilities_data
     @cap = get_capabilities_only
     @typ = create_type_array
     @amount = create_total_array
     @current = create_curr_users_array
    end

    def create_type_array
      @types = []
      @cap.each do |c|
       if c.key?("info_facility_type")
         @types.push(c["info_facility_type"][0]["value"])
       else 
         @types.push("EMPTY")
       end
      end
     @types
    end

    def create_curr_users_array
      @curr = []
      @cap.each do |c|
       if c.key?("current_users")
         @curr.push(c["current_users"][0]["value"])
       else
         @curr.push(Random.rand(0...300))
       end
      end
      @curr
    end

    def create_total_array
      @am = []
      @cap.each do |c|
       if c.key?("info_total_capacity")
         @am.push(c["info_total_capacity"][0]["value"])
       else
         @total_capacity = Random.rand(50...500)
         @am.push(@total_capacity)
       end
      end
      @am
    end


    def get_capabilities_only
      @capabilities = []
      @capacities.each do |c|
       @capabilities.push(c["capabilities"])
      end
      @capabilities
    end

    #this method gets the json from resource adapter
    def get_Health_Json
     health_equip_list = JSON.parse(JSON.load(open("http://107.170.158.70:3004/discovery/resources?capability=info_facility_type")).to_json)
    end

    #get the total capacity of a facility
    def get_facilities_data
     response = JSON.parse(RestClient.post "http://107.170.158.70:3002/resources/data/last", '{"sensor_value":{"capabilities":["info_total_capacity", "info_facility_type", "current_users"]}'.to_json, :content_type => :json, :application => :json)
     response["resources"]
    end
end    