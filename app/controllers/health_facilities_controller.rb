class HealthFacilitiesController < ApplicationController
    
require 'json'
require 'open-uri'
require 'net/http'
require 'rest-client'
require 'mime-types'
require 'netrc'
require 'http-cookie'

  def index
    #@health_equip_hash = get_Health_Equip_Json["resources"]
    @redir = (session[:search] != params[:search]) || (session[:typeOf] != params[:typeOf])
    if params[:search] != nil
      session[:search] = params[:search]
    end
    if params[:typeOf] != nil
      session[:typeOf] = params[:typeOf]
    end
    @all_types = types_to_show
    @checked_types = checked_types_of_facilities
    @health_facilities = get_health_facilities(checked_types_of_facilities)
    @user_address = get_user_address
    @user_coordinates = get_user_coordinates(get_user_address)
    @geocoder_user_address = Geocoder.address(@user_coordinates)
    @hash = create_facilities_markers(order_facilities(get_user_coordinates(get_user_address), @health_facilities))
    @user_markers = create_user_marker(@user_coordinates)
    @winner = order_facilities(@user_coordinates, @health_facilities).first.first
    @winner_coords = [@winner[:lat], @winner[:longi]]
    if @redir then redirect_to health_facilities_path(:search => session[:search], :typeOf => session[:typeOf]) end
  end

  def checked_types_of_facilities
    if params.has_key?(:typeOf)
      if types_to_show.include? params[:typeOf][0]
        return params[:typeOf][0]
      end
    end
    types_to_show
  end
      
  def create_facilities_markers(distances)
    Gmaps4rails.build_markers(distances) do |health_facility, marker|
      marker.lat health_facility[0][:lat]
      marker.lng health_facility[0][:longi]
      marker.picture health_facility[7]
      marker.infowindow "<p><b>#{health_facility[2]}</b></p>
                          <p>Facility type: #{health_facility[0][:type_of]}</p>
                          <p>Distance: #{health_facility[3].round(2).to_s}km</p>
                          <p>Total capacity: #{health_facility[4].to_s}</p>
                          <p>Current users: #{health_facility[5].to_s}</p>
                          <p>Use ratio: #{health_facility[6].round(4).to_s}</p>"
    end
  end
  
  def create_user_marker(user_coordinates)
    [{:lat => user_coordinates[0], :lng => user_coordinates[1], :infowindow => "<p><b>Hidey-ho! I'm the user!</b></p>Address: #{@geocoder_user_address}</p>",
      :picture => {:url => icons_url[:green], :width => 40, :height => 40}}]
  end
  
  def get_user_address
    if params[:search] then params[:search] else default_address end
  end

  def get_user_coordinates(address)
    Geocoder.coordinates(address)
  end
      
    #get the total capacity of a facility
    def get_total_capacity
        response = JSON.parse(RestClient.post "http://107.170.158.70:3002/resources/data/last", '{"sensor_value":{"capabilities":"info_total_capacity"}}'.to_json, :content_type => :json, :application => :json)
        response["resources"]
    end
    #get the facilities available types
    def get_facilities_types
        response = JSON.parse(RestClient.post "http://107.170.158.70:3002/resources/data/last", '{"sensor_value":{"capabilities":"info_facility_type"}}'.to_json, :content_type => :json, :application => :json)
        response["resources"]
    end
    #get the current users of facilities
    def get_current_users
        response = JSON.parse(RestClient.post "http://107.170.158.70:3002/resources/data/last", '{"sensor_value":{"capabilities":"current_users"}}'.to_json, :content_type => :json, :application => :json)
        response["resources"]
    end
      
  #this method gets the json from resource adapter
  def get_Health_Equip_Json
      #json_response = "http://cpro37690.publiccloud.com.br:3004/discovery/resources?capability=info_facility_type"
      #health_equip_list = JSON.parse(JSON.load(open("http://cpro37690.publiccloud.com.br:3004/discovery/resources?capability=info_facility_type")).to_json)
      health_equip_list = JSON.parse(JSON.load(open("http://107.170.158.70:3004/discovery/resources?capability=info_facility_type")).to_json)
  end
      
  #TODO: create url to do a query for facilities using discovery service
  def create_url_query(types_of_facilities)
  end
  
  #TODO: once discovery query is available, modify this function
  #to convert the json obtained from query to the array with format below
  #as this array is being used to create map markers, calculate distances etc.
  #{:lat=> -23.522787, :longi=> -46.490063, :type_of=> "AMA ESPECIALIDADES", :total_capacity=> random_capacity[0], :current_users=> current_users[0]}
  def query_to_array(query)
    query
  end
  
  #TODO: replace hardcoded facilities with a /get query
  def get_health_facilities(types_of_facilities)
    random_capacity = []
    random_mult = []
    current_users = []
    9.times do |i|
      random_capacity.push(Random.rand(50...200))
      random_mult.push(2 * Random.rand)
      current_users.push((random_mult[i] * random_capacity[i]).floor)
    end
    query = [
      {:lat=> -23.522787, :longi=> -46.490063, :type_of=> "AMA ESPECIALIDADES", :total_capacity=> random_capacity[0], :current_users=> current_users[0]},
      {:lat=> -23.673297, :longi=> -46.773393, :type_of=> "AMA ESPECIALIDADES", :total_capacity=> random_capacity[1], :current_users=> current_users[1]},
      {:lat=> -23.531575, :longi=> -46.651898, :type_of=> "AMBULATORIO DE ESPECIALIDADES", :total_capacity=> random_capacity[2], :current_users=> current_users[2]},
      {:lat=> -23.599363, :longi=> -46.539564, :type_of=> "AMA ESPECIALIDADES", :total_capacity=> random_capacity[4], :current_users=> current_users[4]},
      {:lat=> -23.630816, :longi=> -46.766027, :type_of=> "AMA ESPECIALIDADES", :total_capacity=> random_capacity[6], :current_users=> current_users[6]},
      {:lat=> -23.659403, :longi=> -46.743017, :type_of=> "AMA ESPECIALIDADES", :total_capacity=> random_capacity[7], :current_users=> current_users[7]},
      {:lat=> -23.586723, :longi=> -46.491950, :type_of=> "AMA ESPECIALIDADES", :total_capacity=> random_capacity[8], :current_users=> current_users[8]},
      {:lat=> -23.408249, :longi=> -46.757991, :type_of=> "AMBULATORIO DE ESPECIALIDADES", :total_capacity=> 180, :current_users=> 36},
      {:lat=> -23.497448, :longi=> -46.654057, :type_of=> "AMBULATORIO DE ESPECIALIDADES", :total_capacity=> 217, :current_users=> 79},
      {:lat=> -23.570175, :longi=> -46.575142, :type_of=> "AMBULATORIO DE ESPECIALIDADES", :total_capacity=> 383, :current_users=> 78},
      {:lat=> -23.629079, :longi=> -46.604422, :type_of=> "AMBULATORIO DE ESPECIALIDADES", :total_capacity=> 154, :current_users=> 120},
      {:lat=> -23.49283,  :longi=> -46.600559, :type_of=> "AMBULATORIO DE ESPECIALIDADES", :total_capacity=> 98,  :current_users=> 27},
      {:lat=> -23.476568, :longi=> -46.710942, :type_of=> "AMBULATORIO DE ESPECIALIDADES", :total_capacity=> 120, :current_users=> 61},
      {:lat=> -23.628113, :longi=> -46.645846, :type_of=> "AMBULATORIO DE ESPECIALIDADES", :total_capacity=> 340, :current_users=> 228}, 
      {:lat=> -23.523016, :longi=> -46.651378, :type_of=> "AMBULATORIO DE ESPECIALIDADES", :total_capacity=> 383, :current_users=> 352},
      {:lat=> -23.561088, :longi=> -46.654078, :type_of=> "AMBULATORIO DE ESPECIALIDADES", :total_capacity=> 407, :current_users=> 163},
      {:lat=> -23.652993, :longi=> -46.645012, :type_of=> "AMBULATORIO DE ESPECIALIDADES", :total_capacity=> 147, :current_users=> 142},
      {:lat=> -23.695249, :longi=> -46.65946,  :type_of=> "AMBULATORIO DE ESPECIALIDADES", :total_capacity=> 143, :current_users=> 122},
      {:lat=> -23.548579, :longi=> -46.643979, :type_of=> "AMA ESPECIALIDADES", :total_capacity=> 70,  :current_users=> 59},
      {:lat=> -23.524112, :longi=> -46.716684, :type_of=> "AMA ESPECIALIDADES", :total_capacity=> 165, :current_users=> 82}
      ]
    filtered_query = if types_of_facilities != types_to_show
        query.select do |q|
          q[:type_of] == types_of_facilities
        end
      else
        query
      end
    query_to_array(filtered_query)
  end

  def types_to_show
    IO.read('types_of_facilities').split(",")
  end
  
  def default_address
    "Rua do Matão, 1010"
  end
  
  # From inputs distance between user and some facility
  # and use_ratio (ratio between number of users and nominal capacity of the facility).
  # This can be some arbitrary function, obtained from some regression etc.
  # We're using some arbitrary one, not taking into account any model for this score.
  # Future TODO? Adjust this function to some model.
  def calc_score(distance, use_ratio)
    distance + (use_ratio * use_ratio)
  end
  
  # This function orders facilities accordingly their score, which is obtained from calc_score
  # lower the score of the facility, more indicated it is to the user
  def order_facilities(user_coordinates, facilities)
    distances = facilities.map do |facility|
      distance = Geocoder::Calculations.distance_between(user_coordinates, [facility[:lat], facility[:longi]])
      use_ratio = facility[:current_users].to_f / facility[:total_capacity]
      [facility, calc_score(distance, use_ratio), not_a_winner_info[:string], distance, facility[:total_capacity], facility[:current_users], use_ratio, not_a_winner_info[:picture]]
    end
    distances.sort! do |x, y| x[1] <=> y[1] end
    distances[0][2] = winner_info[:string]
    distances[0][7] = winner_info[:picture]
    distances
  end
  
  private
    def winner_info
      {:string => "I'm the chosen one!", :picture => {:url => icons_url[:orange], :width => 40, :height => 40}}
    end
    
    def not_a_winner_info
      {:string => "Not a winner", :picture => {:url => icons_url[:red], :width => 40, :height => 40}}
    end
    
    def icons_url
      {:green => "https://maps.google.com/mapfiles/ms/icons/green-dot.png",
       :orange => "https://maps.google.com/mapfiles/ms/icons/orange-dot.png",
       :red => "https://maps.google.com/mapfiles/ms/icons/red-dot.png"}
    end
end
