require 'rails_helper.rb'

feature "Visiting the home page" do
  scenario "There is the Smart City description" do
    visit root_path
    expect(page).to have_text("Smart City Health Care")
  end

  scenario "There is the Management of Facilities description" do
    visit root_path
    expect(page).to have_text("Management of Facilities")
  end
    
  scenario "No creation of data" do
    visit root_path
    expect(page).not_to have_text("Create new facility")
  end
end

