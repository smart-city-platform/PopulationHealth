require 'rails_helper.rb'

feature "Visiting the management page" do
  scenario "There is the management page description" do
    visit facility_management_index_path
    expect(page).to have_text("Health Care Facilities Management")
  end
    
  scenario "No option to create data" do
    visit root_path
    expect(page).not_to have_text("Create new facility")
  end   
end