require 'rails_helper'
require 'health_facilities_controller'


describe HealthFacilitiesController, :type => :controller do

  render_views
  let(:page) { Capybara::Node::Simple.new(@response.body) }

  describe "GET 'index'" do
    before { get :index }

    it "Get index page with success" do
      should respond_with(:success)
    end

    it "Render with success" do
      should render_template(:index)
    end

    it "Verify the selector" do
      expect(page).to have_selector("div")
    end
  end

  describe "Creation of class" do
    before { get :index }
    it "Check if the class was instantiated correctly" do
      @myFacility = HealthFacilitiesController.new
      expect(@myFacility).to be_an_instance_of(HealthFacilitiesController)
    end
  end

  describe "Routes" do
    it "Test route to /health_facilities" do
      expect(:get => "/health_facilities").to route_to(:controller => "health_facilities", :action => "index")
    end

    it "Test route to /health_facilities/new" do
      expect(:get => "/health_facilities/new").to route_to(:controller => "health_facilities", :action => "new")
    end

    it "Test route to /health_facilities/:id/edit" do
      expect(:get => "/health_facilities/:id/edit").to route_to(:controller => "health_facilities", :action => "edit", "id"=>":id")
    end

    it "Test route to /health_facilities/:id" do
      expect(:get => "/health_facilities/:id").to route_to(:controller => "health_facilities", :action => "show", "id"=>":id")
    end

    it "Test route to Post /health_facilities" do
      expect(:post => "/health_facilities").to route_to(:controller => "health_facilities", :action => "create")
    end

    it "Test route Patch" do
      expect(:patch => "/health_facilities/:id").to route_to(:controller => "health_facilities", :action => "update", "id"=>":id")
    end

    it "Test route Put" do
      expect(:put => "/health_facilities/:id").to route_to(:controller => "health_facilities", :action => "update", "id"=>":id")
    end

    it "Test route Delete" do
      expect(:delete => "/health_facilities/:id").to route_to(:controller => "health_facilities", :action => "destroy", "id"=>":id")
    end

    it "Test route to root" do
      expect(:get => "/").to route_to(:controller => "health_facilities", :action => "index")
    end
  end
  
  describe "Map objects and Geocoder" do
    before(:each) do
      @address = controller.get_user_address
      @coords = controller.get_user_coordinates(@address)
      @markers = controller.create_user_marker(@coords)
    end
    
    it "User address should be a string" do
      expect(@address).to be_instance_of(String)
    end

    it "Should return an array with coordinates" do
      expect(@coords.count).to eq(2)
      expect(@coords[0]).to be_a(Float)
      expect(@coords[1]).to be_a(Float)
    end
    
    it "Should create an array with a single element, which is a hash" do
      expect(@markers.count).to eq(1)
      expect(@markers[0]).to be_instance_of(Hash)
    end
  end
  
  describe "Algorithm to select indicated facility" do
    before(:each) do
      @score = controller.calc_score(1.0, 1.0)
      @user_addr = "Rua do Matão, 1010"
      @user_coords = controller.get_user_coordinates(@user_addr)
      @facilities = [{:lat=> -23.522787, :longi=> -46.490063, :type_of=> "AMA ESPECIALIDADES", :total_capacity=> 42, :current_users=> 42},
                      {:lat=> -23.673297, :longi=> -46.773393, :type_of=> "AMA ESPECIALIDADES", :total_capacity=> 42, :current_users=> 42}]
      @ordered_facilities = controller.order_facilities(@user_coords, @facilities)
    end
    
    it "Should calculate a score, based on distance to facility and its use ratio" do
      expect(@score).to be_instance_of(Float)
    end
    
    it "Should get an array of objects with facilities etc" do
      expect(@ordered_facilities).to be_instance_of(Array)
    end
  end
  
end

