require 'rails_helper'
require 'facility_management_controller'


describe FacilityManagementController, :type => :controller do
  render_views
  let(:page) { Capybara::Node::Simple.new(@response.body) }

  describe "GET 'index' on Facilities Management" do
    before  do
       get :index
    end

    it "Get index page with success" do
      should respond_with(:success)
    end

    it "Render with success" do
      should render_template(:index)
    end

    it "Render with not responding to other request type" do
      expect(page).not_to respond_to('302')
    end
  end

  describe "Test the random number generator" do
    before  do
     get :index
    end

    it "Test the instantiation of the class" do
      @management_controller = FacilityManagementController.new
      expect(@management_controller).to be_an_instance_of(FacilityManagementController)
    end
  end

  describe "Routes of facility management" do

    it "Test route to /facility_management" do
      expect(:get => "/facility_management").to route_to(:controller => "facility_management", :action => "index")
    end

    it "Test route to /facility_management/new" do
      expect(:get => "/facility_management/new").to route_to(:controller => "facility_management", :action => "new")
    end

    it "Test route to /facility_management/:id/edit" do
      expect(:get => "/facility_management/:id/edit").to route_to(:controller => "facility_management", :action => "edit", "id"=>":id")
    end

    it "Test route to /facility_management/:id" do
      expect(:get => "/facility_management/:id").to route_to(:controller => "facility_management", :action => "show", "id"=>":id")
    end

    it "Test route to Post /facility_management" do
      expect(:post => "/facility_management").to route_to(:controller => "facility_management", :action => "create")
    end

    it "Test route Patch facility management" do
      expect(:patch => "/facility_management/:id").to route_to(:controller => "facility_management", :action => "update", "id"=>":id")
    end

    it "Test route Put facility management" do
      expect(:put => "/facility_management/:id").to route_to(:controller => "facility_management", :action => "update", "id"=>":id")
    end

    it "Test route to delete" do
      expect(:delete => "/facility_management/:id").to route_to(:controller => "facility_management", :action => "destroy", "id"=>":id")
    end

  end
end